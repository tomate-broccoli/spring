package sample;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTestContextBootstrapper;
import org.springframework.test.context.BootstrapWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.Profile;

@DisplayName("コンストラクタパターンを動かすテスト")
@ExtendWith(SpringExtension.class)
// @SpringBootTest
// @SpringJUnitConfig(TestConfig.class)
// @EnableAutoConfiguration
// @BootstrapWith(SpringBootTestContextBootstrapper.class)
// @ContextConfiguration(classes = Sample01Test.Config.class) 
class Sample01Test{

    @org.springframework.boot.autoconfigure.SpringBootApplication
    @Configuration
    @ComponentScan
    // @PropertySource("classpath:application.yaml")
    // @TestPropertySource(locations="classpath:application.yml")
    //NG: @ImportResource("classpath:application.yml")
    // @Profile("default")
    public static class Config{}

    @Test
    @DisplayName("何らかのテスト2")
    void test01() {
        // try (GenericApplicationContext applicationContext = new AnnotationConfigApplicationContext(TestConfig.class)) {
        // try (GenericApplicationContext applicationContext = new AnnotationConfigApplicationContext(TestConfig.class, Sample01.class)) {
        // try (GenericApplicationContext applicationContext = new AnnotationConfigApplicationContext(Sample01.class)) {
        // try (AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(Sample01Test.Config.class, Sample01.class)) {
        try (AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext()){
            applicationContext.scan("sample");
            applicationContext.register(Sample01Test.Config.class, Sample01.class);
            applicationContext.refresh();
            Sample01 sample01 = applicationContext.getBean(Sample01.class);
            System.out.println("** sample01.getTxt(): "+ sample01.getText() +".");
            // assertEquals("文字列てすと", sample01.getText());
            assertEquals("hoge", sample01.getText());
        }
    }
}
