package sample;

// import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
// import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.GenericApplicationContext;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
// import org.springframework.boot.test.context.ConfigDataApplicationContextInitializer;
// import org.springframework.test.context.ContextConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.env.YamlPropertySourceLoader;
import org.springframework.core.io.ClassPathResource;
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;

// @Configuration
// @ComponentScan
public class AppMain {
    public static void main2(String[] args) {
        try (GenericApplicationContext applicationContext = new AnnotationConfigApplicationContext(AppMain.class)) {
            Sample01 sample01 = applicationContext.getBean(Sample01.class);
            System.out.println("** sample01.getText(): "+ sample01.getText() + ".");
        }
    }

    public static void main (String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Config.class);
        ClientBean bean = context.getBean(ClientBean.class);
        bean.doSomething();
    }

    // @SpringBootApplication
    @Configuration
    //OK: @PropertySource(value="classpath:hogehoge.properties", encoding="utf-8")
    //OK: @PropertySource(value="classpath:application.properties", encoding="utf-8")
    // @PropertySource(value="classpath:application.properties", encoding="utf-8")
    @ComponentScan("sample")
    public static class Config {
        @Bean
        public ClientBean clientBean () {
            return new ClientBean();
        }

        // @Bean
        // public static PropertySourcesPlaceholderConfigurer placeholderConfigurer() {
        //     return new PropertySourcesPlaceholderConfigurer();
        // }

        @Bean
        public static PropertySourcesPlaceholderConfigurer properties() {
            PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer = new PropertySourcesPlaceholderConfigurer();
            YamlPropertiesFactoryBean yaml = new YamlPropertiesFactoryBean();
            yaml.setResources(new ClassPathResource("application.yaml"));
            propertySourcesPlaceholderConfigurer.setProperties(yaml.getObject());
            return propertySourcesPlaceholderConfigurer;
        }

    }

    public static class ClientBean {
        @Value("${foo.bar}")
        private String currency;    // ${foo.bar}がそのまま設定されてしまっている...

        public void doSomething () {
            System.out.printf("** The currency from prop file is %s%n", currency);
        }
    }

}