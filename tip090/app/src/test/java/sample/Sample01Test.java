package sample;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTestContextBootstrapper;
import org.springframework.test.context.BootstrapWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

@DisplayName("コンストラクタパターンを動かすテスト")
// @ExtendWith(SpringExtension.class)
// @SpringBootTest
@SpringJUnitConfig(TestConfig.class)
// @EnableAutoConfiguration
@BootstrapWith(SpringBootTestContextBootstrapper.class)
class Sample01Test{

    private final Sample01 sample01;

    private final String hoge;

    @Autowired
    Sample01Test(Sample01 sample01) {
        this.sample01 = sample01;
        hoge = "文字列";
    }

    @Test
    @DisplayName("何らかのテスト")
    void test01() {
       assertEquals(hoge, this.sample01.getText());
    }
}
