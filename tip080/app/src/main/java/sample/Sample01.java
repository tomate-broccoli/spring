package sample;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Sample01 {

    // @Value("${foo.bar:hoge}")
    String value;

    public Sample01(@Value("${foo.bar:hoge}") String value){
        this.value = value;
    }

    public String getText(){
        // System.out.println("** Sample01.getText(): value: "+ value + ".");
        return value;
    }
}
