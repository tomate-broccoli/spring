package sample;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.GenericApplicationContext;

@Configuration
@ComponentScan
public class AppMain {
    public static void main(String[] args) {
        try (GenericApplicationContext applicationContext = new AnnotationConfigApplicationContext(AppMain.class)) {
            Sample01 sample01 = applicationContext.getBean(Sample01.class);
            System.out.println("** sample01.getText(): "+ sample01.getText() + ".");
        }
    }
}