package sample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.context.ApplicationContext;

public class AppMain {
    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(AppConfig.class);
        app.setWebApplicationType(WebApplicationType.NONE);
        ApplicationContext ctx = app.run(args);
        Sample01 samp =ctx.getBean(Sample01.class);
        System.out.println("** getText(): "+ samp.getText() + ".");     // ** getText(): 文字列.
    }
}