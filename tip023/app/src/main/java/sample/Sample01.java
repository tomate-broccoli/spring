package sample;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Sample01 {

    @Value("${foo.bar}")
    String value;

    public String getText(){
        return value;
    }
}
