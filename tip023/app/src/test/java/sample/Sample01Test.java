package sample;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTestContextBootstrapper;
import org.springframework.test.context.BootstrapWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.GenericApplicationContext;

@DisplayName("@Value()テスト")
@ExtendWith(SpringExtension.class)
@SpringBootTest
// @SpringJUnitConfig(TestConfig.class)
// @EnableAutoConfiguration
// @BootstrapWith(SpringBootTestContextBootstrapper.class)
class Sample01Test{

    @Autowired
    Sample01 sample01;

    @Test
    @DisplayName("テスト01")
    void test01() {
        assertEquals("文字列てすと", sample01.getText());
    }
}
